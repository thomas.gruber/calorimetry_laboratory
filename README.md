# Lerneinheit II-II: Kalorimetrie (Präsenz)
## Einführung
Siehe Skript und Aufgabenstellung in [moodle](https://moodle.tu-darmstadt.de/course/view.php?id=36368&section=5#tabs-tree-start).

## Materialien
In diesem GitLab Repo finden Sie:
- Package functions (`functions/`): Beinhaltet die Module `m_labor`, `utility` und Skript `s_uuid6`
- Modul `m_labor` (`functions/m_labor.py`): Vorlage der Funktion zum Abspeichern der Einstellung des Labornetzteiles sowie die Zeit der Erwärmung.
- Modul `utility` (`functions/utility.py`): Vorlage der Funktionen zur Datenauswertung
- Skript `s_uuid6` (`functions/s_uuid6.py`): Um UUIDs zu generieren
- Python-Hilfsdatei (`functions/__init__.py`): Notwendige Datei für die Erzeugung eines Python-Pakets
- Datenblätter (`datasheets/`): Vorlage der Datenblätter der Komponenten des Prüfstandes im JSON-Format
- Messdaten (`data/`): Wo die Messdaten abgespeichert werden
- Abbildungsordner (`figures/`): Ordner zum Ablegen die Fotos bzw. Abbildungen aus der Auswertung
- Notebook Laborversuch (`ausarbeitung_laborversuch.ipynb`): Vorlage zur Messdatenauswertung
- Main Skript (`main.py`): Zum Aufnahmen der Messdaten
- Infosblatt (`DS18B20_Datasheet.pdf`): Infosblatt des DS18B20 Sensors
- Readme (`REAMDE.md`): diese Datei
- Requirements (`requirements.txt`): Beschreibt die pip-Umgebung, nicht relevant für die Ausarbeitung
- Matplotlib Style (`FST.mplstyle`): Einstellung für Matplotlib nach der FST-Institut-Vorschrift

### Numpy Quick Start Quide
`numpy.ndarray` ist eine sehr effiziente Datenstruktur im Python-Package `numpy`, die von Datenwissenschaftlern jeden Tag gebraucht wird. Einige Beispiele werden hier gezeigt.

Initializierung von `numpy.ndarray`:
```python
import numpy as np

a = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
```
Anders als `list` müssen die Elemente im `ndarray` von gleichen Typen (zum Beispiel `float64`) sein. Und das Array muss wie eine Matrix in der Mathematik aussehen. Das heißt, die Anzahl der Elemente bei jeder Spalte bzw. Zeile gleich ist.

`numpy.ndarray` erleichert die mathematische Berechnung:
```python
# Assuming that each element in "a" is the radius of a circle,
# the area of each circle can be calculated in this way.
area = np.pi * a ** 2
# Print the array.
print(area)
# Print a element in the array.
print(area[0, 3])
```
Es ist möglich, Statistik des Arrays durch Build-in Funktionen von `numpy` zu brechen.
```python
a_mean = a.mean()
print(a_mean)
# Calculate the average of each column.
a_mean_first_dimension = a.mean(0)
print(a_mean_first_dimension)
```

### Links
Mehr Infomationen über die Datenstruktur sind in der [README.md](https://git.rwth-aachen.de/fst-tuda/public/lehre/calorimetry_home/-/blob/main/README.md) des Küchentischversuches zu finden.

[NumPy: the absolute basics for beginners](https://numpy.org/doc/stable/user/absolute_beginners.html)

[h5py Quick Start Guide](https://docs.h5py.org/en/stable/quick.html)

[NumPy Fundamentals](https://numpy.org/doc/stable/user/basics.html)

[Matplotlib Pyplot Scatter](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.scatter.html#matplotlib.pyplot.scatter)

[PyPi: W1ThermSensor](https://pypi.org/project/w1thermsensor/)

## Ausarbeitung
Die Ausarbeitung erfolgt in den Modul `m_labor` und Notebook `ausarbeitung_laborversuch.ipynb`. In diesen ist bereits eine Gliederung vorgegeben.

## Abgabe
Die Abgabe erfolgt über [moodle](https://moodle.tu-darmstadt.de/mod/assign/view.php?id=1249200). Committen und pushen Sie zunächst Ihre Änderungen auf GitLab und laden Sie von dort Ihr gesamtes Repo als .zip-Datei herunter (ein direkter Download vom JupyterHub ist leider nicht möglich). Benennen Sie die .zip-Datei nach dem folgenden Schema:

<p style="text-align: center;"> &lt;Nachname&gt;_&lt;Vorname&gt;_&lt;MATR-NR&gt;_&lt;GRUPPEN-NR&gt;_le_2-2.zip</p>

Abgaben, die diese Namenskonvention nicht erfüllen, können in der Bewertung nicht berücksichtigt werden.
Laden Sie diese .zip-Datei in moodle hoch. Insbesondere sollten vorhanden sein:
- Jupyter Notebook mit Datenauswertungen
- Python-Modul