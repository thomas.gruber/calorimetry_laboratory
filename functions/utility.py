from typing import Dict, List, Tuple

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np


def plot_temp_over_time(
    data: List[np.ndarray],
    time: List[np.ndarray],
    legend: List[str],
    x_label: str,
    y_label: str,
) -> None:
    """Plots temperature data over time with error bars for multiple datasets.

    This function creates a plot representing temperature data against time
    for multiple sensors or datasets. Each dataset's standard deviation is visualized
    with error bars.

     Args:
        data (List[np.ndarray]): A list of numpy arrays where each array represents
                                 the temperature data (with standard deviation).
                                 Each array should have a shape of (2, n), with n
                                 representing the number of data points.
        time (List[np.ndarray]): A list of numpy arrays with the time data corresponding
                                 to each dataset in `data`.
        legend (List[str]): A list of strings that label each dataset in the legend.
        x_label (str): The label for the x-axis (time).
        y_label (str): The label for the y-axis (temperature).

    """
    # init the matplotlib.axes.Axes and matplotlib.figure.Figure Object for later plot
    fig, ax = plt.subplots(1, 1)
    markers = ["o", "^", "2", "p", "D"]
    plot_data = data[0]
    plot_error = data[1]
    for i in range(len(plot_data)):
        # TODO: draw a plot using the ax.errorbar(...) function
        
        ax.errorbar(time[i], plot_data[i], plot_error[i], xerr=None, fmt='', ecolor=None, elinewidth=None, capsize=None, barsabove=False, lolims=False, uplims=False, xlolims=False, xuplims=False, errorevery=1, capthick=None)


     

    
    legend_handles, labels = ax.get_legend_handles_labels()
    legend_handles = [h[0] for h in legend_handles]

    
    
    ax.legend(legend_handles, labels, loc='upper left',bbox_to_anchor=(1,1))
    ax.set_xlabel(x_label, fontdict=None, labelpad=None, loc=None)
    ax.set_ylabel(y_label, fontdict=None, labelpad=None, loc=None)
    

    # DONE #

    ax.ticklabel_format(scilimits=(0, 3))


def get_plot_data_from_dataset(
    data_path: str, group_path: str
) -> Dict[str, np.ndarray]:
    """Get the necessary data from the dataset to plot.

    This function returns the data in a HDF5 file in all subgroups of a group in 'group_path'
    and automatically categorizes and names the data based on the name of the dataset as well as the metadata.

    Args:
        data_path (str): path to HDF5 file.
        group_path (str): path in HDF5 to group.

    Returns:
        dict[str, np.ndarray]: Data for plot in a dict.

    Example:
        Output (example data):
        {
            "temperature": np.array([
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39]
                        ]) -> temperature from each sensor, The first dimension(row) represents the sensor.
            "timestamp": np.array([
                            [0.43, 1.60, 3.05, 4.25],
                            [0.81, 2.13, 3.49, 4.62],
                            [1.34, 2.60, 3.85, 5.08],
                        ]) -> timestamp for each sensor, The first dimension(row) represents the sensor.
            "name": np.array(["sensor_1", "sensor_2", "sensor_3"]) -> name of each sensor should be hier
        }

    """
    temperature = []
    time = []
    name = []

    with h5.File(data_path) as data:
        group = data[group_path]
        subgroups = []
        min_len = None
        start_time = None

        for subgroup in group:
            try:
                dataset_start_time = group[subgroup]["timestamp"][0]
                dataset_len = len(group[subgroup]["timestamp"])
                
                # Find the minimum length of the data set.
                if min_len is None:
                    min_len = dataset_len
                elif dataset_len < min_len:
                    min_len = dataset_len

                subgroups.append(subgroup)

                # Only group with dataset called timestamp will be read.
            except KeyError:
                continue

            # TODO: Find the start time point of the measurement.
        for subgroup in group:
            try:
                dataset_start_time = group[subgroup]["timestamp"][0]
                

                
                if start_time is None:
                    start_time = dataset_start_time
                elif dataset_start_time < start_time:
                    start_time = dataset_start_time                
            
            except KeyError:
                continue


            # DONE #

        for subgroup in subgroups:
            # TODO: Save data in to the lists temperature, time and name.
            # Data for each sensor must have the same length because of np.ndarray will be use in the output.
            try:
                dataset_len = len(group[subgroup]["timestamp"])
                time_array = np.array(group[subgroup]["timestamp"])
                temperature_array = np.array(group[subgroup]["temperature"])
                
                
                # Check if dataset_len_subgroup > min_len
                if dataset_len > min_len:
                    time_array = np.resize(time_array, time_array.size -1)
                    temperature_array = np.resize(temperature_array, temperature_array.size -1)
                
                
                temperature.append(temperature_array)
                time.append(time_array)
                i = data["RawData"]
                j = i[subgroup]
                subgroup_name = j.attrs["name"]
                name.append(subgroup_name)
                
                
            except KeyError:
                continue
  
            # DONE #

    # TODO: return the output dict.
    dict = {
        'temperature' : temperature,
        'time' : time,
        'name' : name
    }
    
    return dict
        



    # DONE #


def cal_mean_and_standard_deviation(data: np.ndarray) -> np.ndarray:
    """Calculating mean and standard deviation for raw data of multiple sensors.

    Args:
        data (np.ndarray): raw data in a 2 dimensional array (m, n), the first dimension should not be 1 if
                           there are multiple measurements at the same time (and place).

    Returns:
        np.ndarray: mean of raw data with standard deviation in a 2D ndarray with shape (2, n).

    """
    # TODO: Calculate the mean and standard deviation of the first dimension and return the result as a
    # two-dimensional (2, n)-shaped ndarray.
    
    times_array = np.arange(0, len(data), 2)
    temps_array = np.arange(1, len(data), 2)
    times = []
    
    for i in times_array:
        b = int(i)
        times.append(data[b])              
    temps = []
    for i in temps_array:
        b = int(i)
        temps.append(data[b])
    
    current_temps = []
    mean_temp = []
    std_temp = []
    j = 0
    
    while j < len(data[0]):
        current_temps = []
        
        for i in temps:
            current_temps.append(i[j])
        
        mean = np.sum(current_temps)/len(current_temps)
        mean_temp.append(mean)
        
        std = np.std(current_temps, axis=None, dtype=None, out=None, ddof=0)
        std_temp.append(std)
        j += 1
    current_times = []
    mean_time = []
    j = 0
    #same for the time
    while j < len(data[0]):
        current_times = []
        for i in times:
            current_times.append(i[j])
        mean = np.sum(current_times)/len(current_times)
        mean_time.append(mean)
        j += 1
        
    #formating and returning results             
    mean_std_temp = [mean_temp,std_temp]  
    antwort = [mean_std_temp, mean_time]     
    return antwort
                                                                                                            
    # DONE #
    """
    mean_werte = np.mean(data, axis = 0)
    std_dev_werte = np.std(data, axis = 0)
    
    result = np.vstack([mean_werte, std_dev_werte])
    
    return result
    
    """
    
    # DONE #


def get_start_end_temperature(
    temperature_data: np.ndarray, threshold: float = 0.05
) -> Tuple[float, float]:
    """Calculates the high and low temperatures from a dataset.

    This function computes the (average of) the highest temperatures and the (average of) the lowest temperatures
    within a given threshold from the maximum and minimum temperatures recorded in the dataset. These are
    considered as the ending and starting temperatures respectively.

    Args:
        temperature_data (np.ndarray): The temperature dataset as a 2D numpy array.
        threshold (float): The threshold percentage used to identify temperatures close to the maximum
                           and minimum values as high and low temperatures respectively. Default to 0.05

    Returns:
        Tuple[float, float]: A tuple containing the average high temperature first and the average low
                             temperature second.

    """
    # TODO: You don't have to implement this function exactly as docstring expresses it, it just gives
    
    # an idea that you can refer to. The goal of this function is to obtain from the data the high and
    # low temperatures necessary to calculate the heat capacity.
    low_temp = min(temperature_data)
    high_temp = max(temperature_data)

    #formating and returning results
    answer = [low_temp, high_temp]
    return answer
    

    # DONE #
